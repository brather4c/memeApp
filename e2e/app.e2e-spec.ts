import { MemeAppPage } from './app.po';

describe('meme-app App', () => {
  let page: MemeAppPage;

  beforeEach(() => {
    page = new MemeAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

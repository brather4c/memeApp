import {Component, OnInit, ElementRef, ViewChild, OnDestroy} from '@angular/core';
import { CollectionService } from "../../share/collection.service";
import { ImgExample } from '../../share/imgExample';
import {HistoryService} from "../../share/history.service";
import {NewsService} from "../../share/news.service";

@Component({
  selector: '<app-show-list></app-show-list>',
  templateUrl: './show-list.component.html',
  styleUrls: ['./show-list.component.css'],
})

export class ShowListComponent implements OnInit, OnDestroy {

  isCopied: boolean = false;
  blockLoad: boolean = false;
  leftList: Array<ImgExample> = [];
  rightList: Array<ImgExample> = [];
  newsData: any;

  @ViewChild('list1Wrap') elementViewLeft: ElementRef;
  @ViewChild('list2Wrap') elementViewRight: ElementRef;

  activeElem: ImgExample;
  adwDetect: boolean;
  data: any;
  count: number = 0;
  showFlag = false;
  hideListFlag = false;
  showNewsFlag: string = '';

  constructor(private service: CollectionService, public history: HistoryService, public news: NewsService) {
    this.adwDetect = null;
    this.service.newListEvent.subscribe((data) => {
      if (data.length){
        this.data = data;
        this.blockLoad = false;
        this.leftList = [];
        this.rightList = [];
        this.addNewExmpl(this.count);
      } else {
        console.log('!!! data length ', data.length);
      }
    }, error => console.log(error));

    this.news.newNewsEvent.subscribe((data) => {
      if (data.length){
        this.newsData = data;
      } else {
        console.log('!!! data length ', data.length);
      }
    }, error => console.log(error));

  }

  ngOnDestroy(){
    console.log('destroy');
  }

  ngOnInit() {
    let list = document.getElementById('list-wrapper');
        list.style.paddingBottom = window.innerWidth <= 480 ? ( window.innerWidth / 2) + 'px' : '265px';

  }

  addNewExmpl(count){
    this.showNewsFlag = '';
    this.showFlag = true;
    var curentList = (this.elementViewLeft.nativeElement.offsetHeight < this.elementViewRight.nativeElement.offsetHeight) ? this.leftList : this.rightList;
        curentList.push(
          new ImgExample(
            this.data[count].mainCat,
            `http://emoeuphoria.com/${this.data[count].path_gif.slice(13)}`,
            `http://emoeuphoria.com/${this.data[count].path_img.slice(13)}`,
            this.data[count].subCat
          )
        );

    this.count++;
  }

  loadImgExml(e, item){

    if (!this.blockLoad ) {
      item.elWidth = (e.path) ? e.path[0].clientWidth : 0;
      item.elHeight = (e.path) ? e.path[0].clientHeight : 0;
      item.offsetPosition = (e.path) ? e.path[0].offsetTop : 0;


      var itemHtmlElem = e.path[0];
          itemHtmlElem.setAttribute('class', 'imgExmpl loaded');

      if (this.count < this.data.length) {
        this.addNewExmpl(this.count);
      } else {
        this.count = 0;
        console.log('finish');
        this.blockLoad = true;
        this.showNews();
      }
    }
  }

  copyUrl(item){

    if (this.activeElem) {
      this.activeElem.active = false;
      this.activeElem = item;
      item.active = true;
    } else {
      item.active = true;
      this.activeElem = item;
    }

    this.history.updateHistoryCollection(item);
  }

  dinamicToggleList(){
    this.hideListFlag = !this.hideListFlag;
  }

  showNews(){
    let diferenceHeight = Math.abs(this.elementViewLeft.nativeElement.offsetHeight - this.elementViewRight.nativeElement.offsetHeight);
    if (this.newsData.length && diferenceHeight > 80) {
      (this.elementViewLeft.nativeElement.offsetHeight < this.elementViewRight.nativeElement.offsetHeight) ? this.showNewsFlag = 'left' : this.showNewsFlag = 'right';
    }
  }

}



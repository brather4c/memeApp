import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[checkedElem]'
})
export class CheckedElemDirective {

  private checkedEl(border: string) {
    this.el.nativeElement.style.border = border;
  }

  constructor(private el: ElementRef) {}

  @HostListener('click') onClickTap() {
    this.checkedEl('2px solid red');
  }

}

import {Component, OnInit, Output, EventEmitter} from '@angular/core';

import { Gui } from './gui';
import { Differences } from './differences';
import { NavButtons } from './nav-buttons';

declare const Propeller: any;

@Component({
  selector: '<app-radial-nav></app-radial-nav>',
  template: `
      <div id="rotate">
          <svg id="gui" #guiBig></svg>
      </div>
      <svg id="mainType"></svg>
      <app-finger-center (tapElem)="tapFingerCenter()"></app-finger-center>
  `,
  styleUrls: ['./radial-nav.component.css'],
  host: {
    '[style.display]': "'block'",
    '[style.pointerEvents]': "'none'",
    '[style.position]': "'absolute'",
    '[style.height]': 'heightNav',
    '[style.width]': 'heightNav',
    '[style.top]': 'topPosNav'
  }
})


export class RadialNavComponent implements OnInit {
  @Output() sendParams = new EventEmitter();

  public getData;

  mainCat: string = '';
  subCat: string = '';

  public iconsPathHappy:string = './assets/happy_pack.svg';
  public iconsPathConfused:string = './assets/confused_pack.svg';
  public iconsPathSad:string = './assets/sad_pack.svg';
  public iconsMainPath:string = './assets/main_type.svg';



  public smallDiff: Differences = new Differences(180, 0.66, 0.2, false);
  public bigDiff: Differences = new Differences(360, 1, 0.55, true);

  winWidth: number = window.innerWidth >= 480 ? 480 : window.innerWidth;
  heightNav:string;
  topPosNav: string;
  guiBigHappy: Gui;
  guiBigSad: Gui;
  guiBigConfused: Gui;
  guiSmall: Gui;

  constructor(){
    this.heightNav = this.winWidth + 'px';
    this.topPosNav = (window.innerHeight - this.winWidth/2) + 'px';
  }

  tapFingerCenter(){
    this.guiSmall.nav.show();
  }

  ngOnInit() {
    let test = this.mainCat;
    let guiElem = document.getElementById('gui');
    let mainElem = document.getElementById('mainType');
    let getId = (id, e)=> {
      this.subCat = id;
      this.sendParams.emit();
    };

    new Propeller(document.getElementById('rotate'), {inertia: 1, speed: 0});

    this.guiBigHappy = new Gui([
          new NavButtons('addHappy', getId),
          new NavButtons('happy', getId),
          new NavButtons('lol', getId),
          new NavButtons('smile', getId),
          new NavButtons('kiss', getId),
          new NavButtons('lovely', getId),
          new NavButtons('wink', getId),
          new NavButtons('wonderful', getId),
          new NavButtons('joke', getId)
        ],
        guiElem,
        this.iconsPathHappy,
      this.bigDiff);

    this.guiBigSad = new Gui([
        new NavButtons('addSad', getId),
        new NavButtons('deadly', getId),
        new NavButtons('angry', getId),
        new NavButtons('tired', getId),
        new NavButtons('sick', getId),
        new NavButtons('sad', getId),
        new NavButtons('grief', getId),
        new NavButtons('meh', getId),
        new NavButtons('pained', getId)
      ],
      guiElem,
      this.iconsPathSad,
      this.bigDiff);

    this.guiBigConfused = new Gui([
        new NavButtons('addConfused', getId),
        new NavButtons('exhausted', getId),
        new NavButtons('killed', getId),
        new NavButtons('boring', getId),
        new NavButtons('silent', getId),
        new NavButtons('stunned', getId),
        new NavButtons('shocked', getId),
        new NavButtons('playful', getId),
        new NavButtons('comfort', getId)
      ],
      guiElem,
      this.iconsPathConfused,
      this.bigDiff);

    this.guiSmall = new Gui([
      {
        icon: 'happy',
        action: () => {
          this.mainCat = 'happy';
          this.guiBigSad.nav.hide(0);
          this.guiBigConfused.nav.hide(0);
          this.guiBigHappy.nav.show();
        }
      },
      {
        icon: 'sad',
        action: () => {
          this.mainCat = 'sad';
          this.guiBigHappy.nav.hide(0);
          this.guiBigConfused.nav.hide(0);
          this.guiBigSad.nav.show();
        }
      },
      {
        icon: 'confused',
        action: () => {
          this.mainCat = 'confused';
          this.guiBigSad.nav.hide(0);
          this.guiBigHappy.nav.hide(0);
          this.guiBigConfused.nav.show();
        }
      }
    ],
      mainElem,
      this.iconsMainPath,
      this.smallDiff);
  }

}


import { Injectable } from '@angular/core';
declare const Snap: any;
declare const mina: any;

@Injectable()
export class RadialNav {

  winWidth: number = window.innerWidth >= 480 ? 480 : window.innerWidth;
  area: any;
  size: number;
  c: number;
  r: number;
  r2: number;
  angle: number;
  container: any;
  coeffOuter: any;
  aroundAngle: number;
  btn: any;
  animDuration: number;
  animFlag: boolean;

  constructor(paper: any, buttons: any, icons: any, deg: number, coeffInner: number, coeffOuter: number, animType: boolean) {
    // console.log(deg)
    this.animFlag = animType;
    this.size = this.winWidth/2;
    this.area = paper
      .svg(0,0,this.size,this.size)
      .addClass('radialnav');
    this.c = this.size; //Center
    this.r = this.size * coeffOuter; // Outer Radius
    this.r2 = this.r * coeffInner;// Inner Radius
    this.angle = deg / buttons.length;
    this.container = this.area.g();
    this.container.transform('s0');
    this.coeffOuter = coeffOuter;
    this.animDuration = 250;

    this.updateButtons(buttons, icons);

  }

  private _animateButtonHover(button: any, start:number, end:number, duration:any, easing:any, cb:any) {
    this.animate(button, 1, start, end, duration, easing, ((val:any) => {
      button[0].attr({
        d: this.describeSector( this.c, this.c, this.r - val * 10, this.r2, 0, this.angle)
      });
    }), cb);
  }

  private _sector(){
    return this.area
      .path( this.describeSector(this.c, this.c, this.r, this.r2, 0, this.angle))
      .addClass('radialnav-sector');

  }

  private _button(btn:any, sector:any, icon: any, hint: any){
    return this.area
      .g(sector, icon, hint)
      .hover( function(){
        let elems = [this[0], this[1], this[2]];
        for( var i=0; elems.length>i; i++){
          elems[i].toggleClass('active');
        }
      })
      .hover(this._buttonOver(this), this._buttonOut(this))
      .click((e)=> {
        //HIDE NAV HEARE
        btn.action(e);
      })
  }

  private _icon(btn: any, icons: any){
    let icon = icons.select(`#${btn.icon}`).addClass('radialnav-icon');
    let bbox = icon.getBBox();
    const winWidthCoef = ( this.winWidth > 330 ) ? 3 : 4.7;

    let trans = (this.animFlag) ? `T${this.c - bbox.width/2}, ${-30 }R${this.angle/2}, ${this.c}, ${this.c}S.4` : `T${this.c - bbox.width/2}, ${ this.r/winWidthCoef }R${this.angle/2}, ${this.c}, ${this.c}S.4`;
    icon.transform(trans);
    return icon;
  }

  private _hint(btn: any){

      let hint_text = btn.icon;
      if(hint_text == 'addSad') {
        hint_text = 'funny Category';
      } else if (hint_text == 'addHappy'){
        hint_text = 'funny Category';
      } else if (hint_text == 'addConfused') {
        hint_text = 'funny Category';
      }

      let hint = this.area
        .text(0,0, hint_text)
        .addClass('radialnav-hint hide')
        .attr({
          textpath: this.describeArc(this.c, this.c, this.r + 5, 0, this.angle, false)
        });
      hint.select('*').attr({
        startOffset: '50%'
      });

      return hint;
  }

  private _buttonOver(nav) {
    return function () {
      nav._animateButtonHover(this, 0, 1, 200, mina.easeinout);
      this[2].removeClass('hide');
    }
  }

  private _buttonOut(nav){
    return function () {
      nav._animateButtonHover(this, 1, 0, 200, mina.elastic, (()=>{
        this[2].addClass('hide');
      }).bind(this[2]));
    }
  }

  public updateButtons = (buttons: any, icons: any) => {
    this.container.clear();
    let i:number, j:number, len:any;
    for (i = j = 0, len = buttons.length; j < len; i = ++j) {
      let btn = buttons[i];
      let button = this._button(btn, this._sector(), this._icon(btn, icons), this._hint(btn));
      button.transform(`r${this.angle * i}, ${this.c}, ${this.c}`);
      this.container.add(button);
    }
  }

  public show(){
    this.animateContainer(0, 1, this.animDuration * 8, mina.elastic);
  }

  public hide(speed){
    this.animateContainer(1, 0, speed, mina.easeinout);
  }


  public animateContainer = (start:number, end:number, duration:any, easing:any) => {
    this.animate(this, 0, start, end, duration, easing, ((val)=>{
      return this.container.transform((()=>{
        let transformVal;
        if (this.animFlag) {
          transformVal = `r${90-90 * val},${this.c},${this.c},`+`s${val},${val},${this.c},${this.c}`;
        } else {
          transformVal = `s${val},${val},${this.c},${this.c}`;
        }
        return transformVal;
      })());
    }), 0);
  }

  public polarToCartesian = (cx:number, cy:number, r:number, angle:number) => {
    var angle = (angle - 90) * Math.PI / 180; // Degrees to radians
    return {
      x: cx + r * Math.cos(angle),
      y: cy + r * Math.sin(angle)
    }
  }

  public describeArc = (x:number, y:number, r:number, startAngle:number, endAngle:number, continueLine: boolean) => {
    let start = this.polarToCartesian(x, y, r, startAngle %= 360);
    let end = this.polarToCartesian(x, y, r, endAngle %= 360);
    let large = Math.abs(endAngle - startAngle) >= 180;
    let alter = endAngle > startAngle;
    return `${ continueLine ? 'L': 'M'}${start.x},${start.y}A${r},${r},0,${large ? 1 : 0},${ alter ? 1:0},${end.x}, ${end.y}`;
  }

  public describeSector = (x:number, y:number, r:number, r2: number, startAngle:number, endAngle:number) => {
    return `${this.describeArc(x,y,r,startAngle, endAngle, false)} ${this.describeArc(x,y,r2,endAngle, startAngle, true)}Z`
  }

  public animate = (obj:any, index:number, start:number, end:number, duration:number, easing:any, fn:any, cb:any) => {
    // console.log(this);
    if(!obj.animation) obj.animation = [];
    if (obj.animation[index]) obj.animation[index].stop();
    obj.animation[index] = Snap.animate(start, end, fn, duration, easing, cb);
  }

}


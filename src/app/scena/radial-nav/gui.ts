import { Injectable } from '@angular/core';
declare const Snap: any;
import { RadialNav } from './navigation';

@Injectable()
export class Gui {

  public paper: any;
  public nav: any;
  public winWidth: number;
  public deg: number;
  public coeffInner: number;
  public coeffOuter: number;
  public animType: boolean;


  constructor(buttons: any, elemSelector: any, iconsPath: string, diff: any){

    this.deg = diff.deg;
    this.coeffInner = diff.coeffInner;
    this.coeffOuter = diff.coeffOuter;
    this.animType = diff.animationType;
    this.winWidth = window.innerWidth >= 480 ? 480 : window.innerWidth;
    this.paper = Snap(elemSelector).attr({
      width: this.winWidth,
      height: this.winWidth,
    });


    Snap.load(iconsPath, (icons:any)=> {
      this.nav = new RadialNav(this.paper, buttons, icons, this.deg, this.coeffInner, this.coeffOuter, this.animType);
      // this._bindEvents();
    });

    Snap.plugin(function(Snap: any, Element:any){
        Element.prototype.hover = function(f_in:any, f_out:any, s_in:any, s_out:any) {
        return this.mouseover(f_in, s_in)
          .mouseout(f_out || f_in, s_out || s_in);
      }
    });
    //
    // this.paper.node.addEventListener('click', (e)=>{
    //   console.log(e);
    //   console.log(this.paper.node);
    //
    // });
  }

  // public _bindEvents(){
    // window.addEventListener('resize', ()=> {
    //     this.paper.attr({
    //       width: window.innerWidth,
    //       height: window.innerHeight
    //     });
    //   }
    // )
    // console.log(this);
    // this.paper.node.addEventListener('click', this.nav.show.bind(this.nav));
    // this.paper.node.addEventListener('click', this.nav.hide.bind(this.nav));
  // }
}

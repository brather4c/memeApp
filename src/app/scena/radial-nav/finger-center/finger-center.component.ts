import {Component, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-finger-center',
  template: '<i  id="fingerCenter" (click)="onTap()">{{ svgfinger }}</i>',
  styleUrls: ['./finger-center.component.css'],
  host: {
    '[style.display]': "'block'",
    '[style.position]': "'absolute'",
    '[style.background]': "'rgb(207, 72, 88)'",
    '[style.border-radius]': "'100%'",
    '[style.overflow]': "'hidden'",
    '[style.pointerEvents]': "'all'",
    '[style.border]': "'2px solid #fff'",

    '[style.width]': 'elemWidth',
    '[style.height]': 'fingerHeight',
    '[style.left]': 'centred',
    '[style.top]': 'topPosition'
  }
})

export class FingerCenterComponent{

  @Output() tapElem = new EventEmitter();

  fingerHeight: string;
  elemWidth: string;
  centred: string;
  topPosition: string;
  coefPercent: number = .32;
  winWidth: number;

  constructor() {
    this.winWidth = window.innerWidth >=480 ? 480 : window.innerWidth;
    this.elemWidth = this.winWidth * this.coefPercent +'px';
    this.fingerHeight = this.winWidth * this.coefPercent + 'px';
    this.centred = (this.winWidth/2) - (this.winWidth * this.coefPercent)/2 + 'px';
    this.topPosition = (this.winWidth/2 - (this.winWidth * this.coefPercent)/2) + 'px';
  }

  onTap(){
    this.tapElem.emit();
  }
}

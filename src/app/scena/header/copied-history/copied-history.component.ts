import { Component, OnInit } from '@angular/core';
import { HistoryService } from "../../../share/history.service";


@Component({
  selector: 'app-copied-history',
  templateUrl: './copied-history.component.html',
  styleUrls: ['./copied-history.component.css']
})
export class CopiedHistoryComponent implements OnInit {

  historyList: any;
  isCopied: boolean = false;
  showHistory = false;
  activeElem: any;
  circleSize:number;

  constructor( private history: HistoryService) { }

  ngOnInit() {
    this.historyList = this.history.getHistoryCollection().map((item)=>{
      item.active = false;
      return item;
    }).reverse();
    console.log(this.historyList);
    this.showHistory = true;
    this.circleSize = window.innerHeight - 54;
  }

  copyUrl(item) {

    if (this.activeElem) {
      this.activeElem.active = false;
      this.activeElem = item;
      item.active = true;
    } else {
      item.active = true;
      this.activeElem = item;
    }
  }

}

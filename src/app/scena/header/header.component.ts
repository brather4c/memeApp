import { Component, Output, EventEmitter } from '@angular/core';


@Component({
  selector: '<app-header></app-header> ',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  public mainCat: string = 'Main Category';
  public subCat: string = 'Sub Category';

  showHistoryMark = false;
  @Output() flyOffShowList = new EventEmitter();

  showHistory(e, header){
    this.flyOffShowList.emit();
    this.showHistoryMark = !this.showHistoryMark;
    console.log('history');
  }
}


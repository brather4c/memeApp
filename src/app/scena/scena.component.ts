import {Component, OnInit} from '@angular/core';
import { CollectionService } from '../share/collection.service';
import { HeaderComponent } from './header/header.component';
import {HistoryService} from "../share/history.service";
import {NewsService} from "../share/news.service";


@Component({
  selector: 'app-scena',
  template: `
      <app-header #header (flyOffShowList)="offList(test)"></app-header>
      <app-show-list #test></app-show-list>
      <app-radial-nav #nav (sendParams)="sendParams(nav, header)"></app-radial-nav>
  `,
  styleUrls: ['./scena.component.css'],
  providers:[ HeaderComponent ]
})

export class ScenaComponent implements OnInit {

  // mainCatSave
  constructor(private collectionService: CollectionService, private history:HistoryService, private newsService: NewsService){}

  sendParams(nav, header){
    console.log(nav.subCat, nav.mainCat);

    // this.router.navigate(['/show-list', nav.mainCat, nav.subCat]);

    if (nav.subCat == 'addSad' || nav.subCat == 'addConfused' || nav.subCat == 'addHappy') {
      this.collectionService.getTopCollection();
    } else {
      this.collectionService.getNewCollection(nav.mainCat, nav.subCat);
    }

    header.mainCat = nav.mainCat;
    header.subCat = nav.subCat;

    if (nav.mainCat == 'happy') nav.guiBigHappy.nav.hide(400);
    if (nav.mainCat == 'sad') nav.guiBigSad.nav.hide(400);
    if (nav.mainCat == 'confused') nav.guiBigConfused.nav.hide(400);
  }

  ngOnInit(){
    this.history.getHistoryCollection();
    this.newsService.getNewsPack();
  }

  offList(radNav){
    radNav.dinamicToggleList();
  }

}


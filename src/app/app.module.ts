import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ClipboardModule } from 'ngx-clipboard';


import { AppComponent } from './app.component';
import { ScenaComponent } from './scena/scena.component';
import { HeaderComponent } from './scena/header/header.component';
import { RadialNavComponent } from './scena/radial-nav/radial-nav.component';
import { FingerCenterComponent } from './scena/radial-nav/finger-center/finger-center.component';
import { ShowListComponent } from './scena/show-list/show-list.component';
import { CollectionService } from './share/collection.service';
import { CheckedElemDirective } from './scena/show-list/checked-elem.directive';
import { CopiedHistoryComponent } from './scena/header/copied-history/copied-history.component';
import {HistoryService} from "./share/history.service";
import {NewsService} from "./share/news.service";

@NgModule({
  declarations: [
    AppComponent,
    ScenaComponent,
    HeaderComponent,
    RadialNavComponent,
    FingerCenterComponent,
    ShowListComponent,
    CheckedElemDirective,
    CopiedHistoryComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ClipboardModule
  ],
  providers: [CollectionService, HistoryService, NewsService],
  bootstrap: [AppComponent]
})
export class AppModule { }

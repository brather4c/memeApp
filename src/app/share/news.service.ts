import { Injectable, EventEmitter } from '@angular/core';
import { Http, Response } from '@angular/http';

@Injectable()
export class NewsService {

  APIKey = 'IKNOWSECRETPATH2805-1';
  constructor(private _http: Http){}

  newNewsEvent: EventEmitter<any> = new EventEmitter();

  getNewsPack(){
    return this._http.get(`http://emoeuphoria.com/adwer/needadvertising/${this.APIKey}/100+0`)
      .map((res:Response) => res.json())
      .subscribe((data) => this.newNewsEvent.emit(data),
        error => alert(error),
        ()=> console.log('success get data')
      );
  }


}


import { Injectable } from '@angular/core';

@Injectable()
export class HistoryService {

  historyCollection = [];

  getHistoryCollection(){
    this.historyCollection = [];
    if(localStorage.getItem('history')){
      JSON.parse(localStorage.getItem('history')).map(item=> {
        this.historyCollection.push(item);
      });
    }

    return this.historyCollection;
  }

  public updateHistoryCollection( newItem ){
    var unic = true;
    if (this.historyCollection.length) this.historyCollection.map((item)=>(item.path_gif == newItem.path_gif) ? unic = false : 1);

    if (unic) {
      if (this.historyCollection.length == 10) this.historyCollection.splice(0,1);
      this.historyCollection.push(newItem);
      localStorage.setItem('history', JSON.stringify(this.historyCollection));
    }

    return this.historyCollection;

  }

}

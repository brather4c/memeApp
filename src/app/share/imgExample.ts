export class ImgExample {

  active: boolean;
  // hideAnimationMark: boolean;

  constructor(public mainCat:string, public path_gif:string, public path_img:string, public subCat:string, public elWidth?: number, public elHeight?: number, public offsetPosition?: number){
    this.mainCat = mainCat;
    this.path_gif = path_gif;
    this.path_img = path_img;
    this.subCat = subCat;
    this.elWidth = elWidth || 0;
    this.elHeight = elHeight || 0;
    this.offsetPosition = offsetPosition || 0;
    this.active = false;
    // this.hideAnimationMark = false;
  }


}

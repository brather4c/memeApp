import { Injectable, EventEmitter } from '@angular/core';
import { Http, Response } from '@angular/http';
const topCat = ['awesome','girls','cat','fails','animals','topcollection'];
@Injectable()
export class CollectionService {

  APIKey = 'IKNOWSECRETPATH2805-1';
  constructor(private _http: Http){}

  newListEvent: EventEmitter<any> = new EventEmitter();

  getNewCollection(mainCat: string, subCat: string){
    return this._http.get(`http://emoeuphoria.com/gallery/${this.APIKey}/${mainCat}/${subCat}&100`)
      .map((res:Response) => res.json())
      .subscribe((data) => this.newListEvent.emit(data),
        error => alert(error),
        ()=> console.log('success get data')
    );
  }

  getTopCollection(){
    const limitCount = 100;

    return this._http.get('http://emoeuphoria.com/gallery/topeuphoria/'+ topCat[this.getRandomCat(topCat.length)] + '&'+ limitCount + '+' + 0)
      .map((res:Response) => res.json())
      .subscribe((data) => this.newListEvent.emit(data),
        error => alert(error),
        ()=> console.log('success get data')
      );
  }

  private getRandomCat(max) {
    var rand = 0 + Math.random() * (max + 1 - 0);
        rand = Math.floor(rand);
    return rand;
  }
}

// var urlAdw = servUrl + '/adwer/needadvertising/'+ APIKey + '/100+0';
